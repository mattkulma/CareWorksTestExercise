using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CareWorksTestExercise;


namespace CareWorksTestExercise.test
{
    [TestClass]
    public class ProgramTests
    {

        [TestMethod()]
        public void LowerCase() //returns numbers 1-26 for each letter
        {
            //arrange
            Program exercise = new Program();

            //act

            //assert
            Assert.AreEqual(123, exercise.ConvertLettersToNumbers("abc"));
            Assert.AreEqual(209101,exercise.ConvertLettersToNumbers("tija"));    
        }

        [TestMethod()]
        public void UpperCase() //returns even numbers 2-52 for each capital letter
        {
            //arrange
            Program exercise = new Program();
            //act

            //assert
            Assert.AreEqual(61252, exercise.ConvertLettersToNumbers("CFZ"));
            Assert.AreEqual(1443822,exercise.ConvertLettersToNumbers("GBSK"));
        }

        [TestMethod()]
        public void MixedCase() //returns numbers 1-52 for each letter
        {
            //arrange
            Program exercise = new Program();
            //act

            //assert
            Assert.AreEqual(1142244, exercise.ConvertLettersToNumbers("aGKdB"));
            Assert.AreEqual(1472363819,exercise.ConvertLettersToNumbers("GgbRSs"));
        }

        [TestMethod()]
        public void NotLetter() //returns -1 for each non letter charecter
        {
            //arrange
            Program exercise = new Program();
            //act

            //assert
            Assert.AreEqual(-1, exercise.ConvertLettersToNumbers("!"));
            Assert.AreEqual(-1,exercise.ConvertLettersToNumbers("!367&*%"));
        }

        [TestMethod()]
        public void AllCharacters() //returns a combination or numbers -1-52 excluding 0
        {
            //arrange
            Program exercise = new Program();
            //act

            //assert
            Assert.AreEqual(-1, exercise.ConvertLettersToNumbers("aB*gF"));
            Assert.AreEqual(-1,exercise.ConvertLettersToNumbers("#BjY%^&8PMn"));
            Assert.AreEqual(-1123, exercise.ConvertLettersToNumbers("!abc"));
            Assert.AreEqual(-1, exercise.ConvertLettersToNumbers("acb%"));
        }

    }
}

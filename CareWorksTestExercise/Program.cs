﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CareWorksTestExercise
{
    public class Program
    {

        public int ConvertLettersToNumbers(string input)
        {

            char[] lower = "abcdefghijklmnopqrstuvwxyz".ToCharArray();
            char[] upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();

            char[] letters = input.ToCharArray();
            List<int> numbers = new List<int>();

            for (int i = 0; i < letters.Length; i++)
            {
                for (int k = 0; k < lower.Length; k++)
                {
                    if (!lower.Contains(letters[i]) && !upper.Contains(letters[i]))
                    {
                        numbers.Add(-1);
                        break;
                    }

                    if (letters[i] == lower[k] && letters[i] != upper[k])
                    {
                        numbers.Add(k + 1);
                    }

                    if (letters[i] == upper[k] && letters[i] != lower[k])
                    {
                        numbers.Add((k + 1) * 2);
                    }
                }
            }

            int number = 0;
            try
            {
                number = int.Parse(string.Join("", numbers)); //doesnt handle non-letter characters
            }
            catch (Exception)
            {
                return -1;
                throw;
            }

            return number;

        }

        static void Main(string[] args)
        {
        }

    }
}
